from time import sleep, time
import numpy as np
from PIL import Image, ImageDraw
from ssd1362 import Ssd1362 
from copy import copy

image_path = './image/tbond-oled.png'

width = 256
height = 64

base_img = Image.open(image_path).resize((width, height))
base_img = base_img.convert("L")

oled = Ssd1362(spibus=0, spidev=0, io_dc=38)

txt = ''
while True:
    start = time()
    img = copy(base_img)
    draw = ImageDraw.Draw(img)
    draw.text((160,50), txt, fill=255)
    frame = np.asarray(img, dtype=np.uint8)
    oled.loadframe(frame)
    oled.show(15)

    elps = time() - start
    txt = f'elps : {elps*1000:02.2f}ms'
    print(txt, end='\r')
    sleep(0.1)